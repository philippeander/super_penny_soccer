﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

public class Coin : MonoBehaviour
{
    static public Coin Instance;

    [SerializeField] private Enums.SecondTouchMode m_secondTouchMode;

    [Space(10)]
    [Header("OTHERs")]
    [SerializeField] private float m_swipeSpeed = 2;
    [SerializeField] private float m_limitOfTimeToShoot = 10000f; //In Seconds (in this kit we give players unlimited time to perform their turn of shooting)
    [SerializeField] private float m_distanceToHelperBegin = 0;
    [SerializeField] private float m_smoothOnDrag = 2;
    [SerializeField] private float m_minSizeCicleArea = .38f;
    [SerializeField] private float power = 5; //shoot power
    

    private Transform m_pointHelperA;
    private Transform m_pointHelperB;
    private Transform m_pointHelperC;


    //private float currentDistance;      //real distance of our touch/mouse position from initial drag position
    private float m_speed;
    private Vector3 shootDirectionVector; //this vector holds shooting direction
    private Rigidbody m_rBody;

    private Vector3 m_initPos;

    private float m_anglePositionDiference = 0;
    private float m_secondFingerAngle = 0;
    [SerializeField] private bool m_isSecondFingerTouched = false;
    [SerializeField] private bool m_isFirstFingerLived = false;


    public Vector3 InitPos { get => m_initPos; set => m_initPos = value; }
    public Rigidbody RBody { get => m_rBody; }

    private void Awake()
    {
        Instance = this; 

        m_pointHelperA = GameObject.FindGameObjectWithTag(Constants.A_POINT_HELPER).GetComponent<Transform>();
        m_pointHelperB = GameObject.FindGameObjectWithTag(Constants.B_POINT_HELPER).GetComponent<Transform>();
        m_pointHelperC = GameObject.FindGameObjectWithTag(Constants.C_POINT_HELPER).GetComponent<Transform>();

        m_rBody = GetComponent<Rigidbody>();
    }

    private void Start()
    {
        InitPos = transform.position;
        RBody.sleepThreshold = 0.15f;

        CanvasArrow.Instance.EnableGraphicsElements(false);
        
    }

    void FixedUpdate()
    {
        FrictionManager();

    }

    public void OnMouseDrag()
    {
        if (GlobalGameManager.CanShoot) {

            CanvasArrow.Instance.transform.position = transform.position;
            CanvasArrow.Instance.EnableGraphicsElements(true);

            // Handle screen touches.
            if (Input.touchCount > 0)
            {

                Touch touch = Input.GetTouch(0);

                if (touch.phase == TouchPhase.Stationary || touch.phase == TouchPhase.Moved)
                {
                    RaycastHit hit;
                    Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);

                    if (Physics.Raycast(ray, out hit, 10000, Constants.GetMaskField))
                    {
                        if (hit.collider != null)
                        {
                            m_pointHelperA.position = Vector3.Lerp(transform.position, hit.point, Time.time);
                            
                        }
                    }
                }else if (touch.phase == TouchPhase.Ended)
                {
                    m_isFirstFingerLived = true;
                    
                }

                if (Input.touchCount == 2)
                {
                    touch = Input.GetTouch(1);

                    if (touch.phase == TouchPhase.Stationary || touch.phase == TouchPhase.Moved)
                    {
                        m_isSecondFingerTouched = true;

                        RaycastHit hit;
                        Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(1).position);

                        if (Physics.Raycast(ray, out hit, 10000, Constants.GetMaskField))
                        {
                            if (hit.collider != null)
                            {
                                m_secondFingerAngle = (CalculateAngle(m_pointHelperC.position) - CalculateAngle(hit.point)) - 180;
                            }
                        }

                        switch (m_secondTouchMode)
                        {
                            case Enums.SecondTouchMode.LOOKING_AT:

                                if (Physics.Raycast(ray, out hit, 10000, Constants.GetMaskField))
                                {
                                    if (hit.collider != null)
                                    {
                                        m_pointHelperC.position = Vector3.Lerp(transform.position, hit.point, Time.time);
                                    }
                                }
                                break;
                            case Enums.SecondTouchMode.SWIPE_GESTURE:

                                float deltaPosX = touch.deltaPosition.x;
                                float deltaPosY = touch.deltaPosition.y;
                                float deltaVal = 0;

                                switch (GetDragDirection(touch.deltaPosition))
                                {
                                    case Enums.DraggedDirection.Up:
                                    case Enums.DraggedDirection.Down:
                                        deltaVal = deltaPosY;
                                        break;
                                    case Enums.DraggedDirection.Right:
                                    case Enums.DraggedDirection.Left:
                                        deltaVal = deltaPosX;
                                        break;
                                }

                                m_pointHelperC.RotateAround(transform.position, Vector3.up, (deltaVal * m_swipeSpeed) * Time.deltaTime);

                                break;
                        }

                        m_anglePositionDiference = CalculateAngle(m_pointHelperC.position) - CalculateAngle(PositionOfHelperEnd());
                    }
                    else if (touch.phase == TouchPhase.Ended) {
                        m_isSecondFingerTouched = false;
                    }
                }
                else {

                    Vector3 offSet = PositionOfHelperEnd() - transform.position;

                    offSet = Quaternion.AngleAxis(m_isFirstFingerLived && m_isSecondFingerTouched ? m_secondFingerAngle : m_anglePositionDiference, transform.up) * offSet;

                    m_pointHelperC.position = transform.position + offSet;

                }

                Vector3 dir = m_pointHelperC.position - transform.position;

                float angleCalc = Vector3.Angle(dir, Vector3.right); // VECTOR RIGHT
                float outRotation = Vector3.Angle(dir, Vector3.forward) * (angleCalc > 90 ? 1 : -1); // VECTOR FORWARD

                CanvasArrow.Instance.arrowPivot.localEulerAngles = new Vector3(0, 0, outRotation);

            }

            m_pointHelperB.position = PositionOfHelperEnd();

            ArrowSliderValue();

            //debug line from initial position to our current touch position
            Debug.DrawLine(transform.position, new Vector3( m_pointHelperA.transform.position.x, transform.position.y, m_pointHelperA.transform.position.z), Color.red);
            //debug line from initial position to the exact opposite position (mirrored) of our current touch position
            Debug.DrawLine(transform.position, new Vector3(m_pointHelperB.transform.position.x, transform.position.y, m_pointHelperB.transform.position.z), Color.yellow);
            //cast ray forward and collect informations

            //final vector used to shoot the unit.
            shootDirectionVector = m_pointHelperC.transform.position;

        }
    }

    private Enums.DraggedDirection GetDragDirection(Vector3 dragVector)
    {
        float positiveX = Mathf.Abs(dragVector.x);
        float positiveY = Mathf.Abs(dragVector.y);

        Enums.DraggedDirection draggedDir;

        if (positiveX > positiveY)
        {
            draggedDir = (dragVector.x > 0) ? Enums.DraggedDirection.Right : Enums.DraggedDirection.Left;
        }
        else
        {
            draggedDir = (dragVector.y > 0) ? Enums.DraggedDirection.Up : Enums.DraggedDirection.Down;
        }

        //Debug.Log(draggedDir);
        return draggedDir;
    }

    private void ArrowSliderValue()
    {
        m_distanceToHelperBegin = Vector3.Distance(transform.position, m_pointHelperA.position);
        m_distanceToHelperBegin = m_distanceToHelperBegin / m_smoothOnDrag;

        CanvasArrow.Instance.slider.value = m_distanceToHelperBegin;

        float calcCircleSize = m_minSizeCicleArea + (CanvasArrow.Instance.slider.value / (1 + m_minSizeCicleArea));
        CanvasArrow.Instance.circlePresure.localScale = new Vector2(calcCircleSize, calcCircleSize);

    }

    private float CalculateAngle(Vector3 TargetPosition)
    {
        var myPos = transform.position;
        myPos.y = 0;

        var targetPos = TargetPosition;
        targetPos.y = 0;

        Vector3 toOther = (myPos - targetPos).normalized;

        float angle = Mathf.Atan2(toOther.z, -toOther.x) * Mathf.Rad2Deg + 180;
        return angle;
        //return angle + 90;
    }

    /// <summary>
    /// Take the opposite position of the first Click
    /// </summary>
    /// <returns></returns>
    private Vector3 PositionOfHelperEnd() {
        Vector3 helperEnd = Vector3.zero;

        float currentDistance = Vector3.Distance(m_pointHelperA.position, transform.position);

        Vector3 dxy = m_pointHelperA.transform.position - transform.position;
        float diff = dxy.magnitude;

        helperEnd = transform.position + ((dxy / diff) * currentDistance * -1);

        helperEnd = new Vector3(helperEnd.x,
                                transform.position.y,
                                helperEnd.z);


        return helperEnd;
    }

    

    public void OnMouseUp()
    {
        CanvasArrow.Instance.EnableGraphicsElements(false);
        m_anglePositionDiference = 0;
        m_isSecondFingerTouched = false;
        m_isFirstFingerLived = false;
        m_secondFingerAngle = 0;

        if (CanvasArrow.Instance.slider.value < .1f || !GlobalGameManager.CanShoot) {
            return;
        }

        //CanShoot = false;

        //do the physics calculations and shoot the ball 
        float calcPower = power * CanvasArrow.Instance.slider.value;
        Vector3 outPower = transform.position - shootDirectionVector;
        outPower.Normalize();
        outPower *= calcPower * -1;

        RBody.AddForce(outPower, ForceMode.Impulse);

    }


    private void FrictionManager() {
        m_speed = RBody.velocity.magnitude;

        RBody.drag = m_speed < .5f ?
                       RBody.drag = 2 :
                       RBody.drag = 0.9f;
        

    }
    public void ToggleSeSecondTouchMode(int val) {
        switch (val) {
            case 1:
                ChangeSecondTouchMode(Enums.SecondTouchMode.LOOKING_AT);
                break;
            case 2:
                ChangeSecondTouchMode(Enums.SecondTouchMode.SWIPE_GESTURE);
                break;
        }
    }
    public void ChangeSecondTouchMode(Enums.SecondTouchMode secondTouchMode) {
        m_secondTouchMode = secondTouchMode;
    }

    private void OnTriggerEnter(Collider other)
    {
        GlobalGameManager.Instance.ManagePostGoal(other.gameObject.tag);

    }

}
