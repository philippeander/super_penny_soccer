using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class mouseFollow : MonoBehaviour
{

	private float yOffset = 0.1f; //fixed position on Z axis.

    void Start (){
		//transform.position = new Vector3(transform.position.x,
  //                                       yOffset,
  //                                       transform.position.z);
		//transform.position.z = zOffset; //apply fixed offset
	}

	private Vector3 tmpPosition;
	void Update (){

        /*
        if (Input.touchCount > 0)
        {
            switch (Input.GetTouch(0).phase)
            {
                case TouchPhase.Began:
                    // code to run when touch begins here...
                    break;
                case TouchPhase.Moved:
                case TouchPhase.Stationary:
                    // code to run when touch is being dragged here...
                    break;
                case TouchPhase.Ended:
                    // code to run when touch is lifted up and finished here...
                    break;
                case TouchPhase.Canceled:
                    // code to run when touch is interrupted and does not get to run the 'Ended' case here... (jumps from dragging to being canceled by some interruption like a phone call)
                    break;
            }
        }
        */
//#if UNITY_ANDROID || UNITY_IOS
        // Handle screen touches.
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);

            if (touch.phase == TouchPhase.Stationary || touch.phase == TouchPhase.Moved)
            {
                RaycastHit hit;
                Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
                

                if (Physics.Raycast(ray, out hit, 10000, Constants.GetMaskField))
                {
                    if (hit.collider != null)
                    {
                        transform.position = Vector3.Lerp(transform.position, hit.point, Time.time);
                    }
                }
            }

        }
//#endif
        /*
        //get mouse position in game scene.
        tmpPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition); 
		//follow the mouse
		transform.position = new Vector3(	tmpPosition.x,
                                            Coin.Instance != null ? Coin.Instance.transform.position.y : yOffset,
                                            tmpPosition.z);
        */
    }

}