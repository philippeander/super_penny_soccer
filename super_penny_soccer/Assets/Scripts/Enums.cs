﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enums
{
    public enum FormationType {
        FM_3_2_1,
        FM_2_3_1,
        FM_2_1_2_1
    }
    public enum SecondTouchMode
    {
        LOOKING_AT,
        SWIPE_GESTURE
    }

    public enum DraggedDirection
    {
        Up,
        Down,
        Right,
        Left
    }
}
