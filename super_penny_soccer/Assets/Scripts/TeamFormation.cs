﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class TeamFormation : MonoBehaviour
{
    [SerializeField] private Enums.FormationType m_formationType;

    [Space(20)]
    [SerializeField] private Transform m_goalkeeper;
    [SerializeField] private Transform m_rightFullBack;
    [SerializeField] private Transform m_leftFullBack;
    [SerializeField] private Transform m_CenterMf;
    [SerializeField] private Transform m_RightMf;
    [SerializeField] private Transform m_LeftMf;

    public Enums.FormationType FormationType { get => m_formationType; }
    public Transform Goalkeeper { get => m_goalkeeper;}
    public Transform RightFullBack { get => m_rightFullBack;}
    public Transform LeftFullBack { get => m_leftFullBack;}
    public Transform CenterMf { get => m_CenterMf;}
    public Transform RightMf { get => m_RightMf;}
    public Transform LeftMf { get => m_LeftMf;}
    public List<Transform> FormationPositions {
        get {
            List<Transform> formationPositions = new List<Transform>();
            formationPositions.Add(m_goalkeeper);
            formationPositions.Add(m_rightFullBack);
            formationPositions.Add(m_leftFullBack);
            formationPositions.Add(m_CenterMf);
            formationPositions.Add(m_RightMf);
            formationPositions.Add(m_LeftMf);
            return formationPositions;
        }
    }

    
}
