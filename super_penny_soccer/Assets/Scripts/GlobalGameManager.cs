﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using System;
using TMPro;
using UnityEngine.UI;

public class GlobalGameManager : MonoBehaviour
{
    [Serializable] public class Player {
        public string playerName = "Player 1";
        public int points = 0;
        public Material UnitMaterial;
        public Transform playerTeamContainer;

        public List<TeamFormation> listFormations;
        [NonSerialized] public TeamFormation curFormationType;

        public List<GameObject> listOfPlayers;

        public void InitFormation(Enums.FormationType formationType = Enums.FormationType.FM_2_1_2_1) {

            curFormationType = GetFormation(formationType);

            for (int i = 0; i < curFormationType.FormationPositions.Count; i++) {

                GameObject newPlayerInit = Instantiate(
                                           GlobalGameManager.Instance.PlayerUnitPrefab, 
                                           curFormationType.FormationPositions[i].position, 
                                           Quaternion.identity,
                                           playerTeamContainer) as GameObject;

                newPlayerInit.GetComponent<Renderer>().material = UnitMaterial;

                listOfPlayers.Add(newPlayerInit);
            }
            
        }

        public TeamFormation GetFormation(Enums.FormationType formationType = Enums.FormationType.FM_2_1_2_1) {
            return listFormations.FirstOrDefault(x => x.FormationType == formationType);
        }

    }

    static public GlobalGameManager Instance;

    static public bool CanShoot = true;
    static public float MaxDistance = 3.0f;

    public static bool goalHappened;
    public static bool shootHappened;
    public static int goalLimit = 5; //To finish the game quickly, without letting the GameTime end.

    public static float m_baseShootTime = 15;

    [Space(10)]
    [Header("PLAYER")]
    [SerializeField] private Player m_playerObj;

    [Space(10)]
    [Header("OPPONENT")]
    [SerializeField] private Player m_OpponentObj;

    [Space(10)]
    [Header("CANVAS")]
    [SerializeField] private GameObject m_goalPanel;
    [SerializeField] private TextMeshProUGUI m_scoreboard;

    [Space(15)]
    [SerializeField] private GameObject m_playerUnitPrefab;
    [SerializeField] private float m_speedFormation = 1.0f;

    [Space(15)]
    [SerializeField] private ToggleGroup m_toggleGroup2ndTouch;


    public GameObject PlayerUnitPrefab { get => m_playerUnitPrefab; }

    private void Awake()
    {
        Instance = this;
        m_goalPanel.SetActive(false);
    }

    private void Start()
    {
        m_playerObj.InitFormation(Enums.FormationType.FM_2_1_2_1);
        m_OpponentObj.InitFormation(Enums.FormationType.FM_2_3_1);

        Toggle toggle = m_toggleGroup2ndTouch.GetComponentInChildren<Toggle>();
        toggle.isOn = true;
    }

    

    public void ManagePostGoal(string goalBy) {
        StartCoroutine(ManagePostGoal_CO(goalBy));
    }

    private IEnumerator ManagePostGoal_CO(string goalBy) {

        switch (goalBy) {
            case Constants.TAG_PLAYER:
                m_playerObj.points++;
                break;
            case Constants.TAG_OPPONENT:
                m_OpponentObj.points++;
                break;
            default:
                Debug.LogWarning("PLAYER NOT FOUND!");
                break;
        }
        Coin.Instance.RBody.velocity = Vector3.zero;
        Coin.Instance.transform.position = Coin.Instance.InitPos;

        m_scoreboard.text = m_playerObj.points + " - " + m_OpponentObj.points;

        m_goalPanel.SetActive(true);

        yield return new WaitForSeconds(3f);

        m_goalPanel.SetActive(false);

        yield return new WaitForEndOfFrame();

    }

    [ContextMenu("CHANGE FORMATION TEST")]
    public void ChangeFormation()
    {
        ChangeFormation(m_playerObj, Enums.FormationType.FM_3_2_1);
        ChangeFormation(m_OpponentObj, Enums.FormationType.FM_2_1_2_1);
    }
    public void ChangeFormation(Player player ,Enums.FormationType newFormationType = Enums.FormationType.FM_2_1_2_1)
    {
        for (int i = 0; i < player.listOfPlayers.Count; i++)
        {
            player.curFormationType = player.GetFormation(newFormationType);
            StartCoroutine(SetObjectToPosition_CO(player.listOfPlayers[i].transform, player.curFormationType.FormationPositions[i].position));
        }
    }
    private IEnumerator SetObjectToPosition_CO(Transform playerUnit, Vector3 targetPos)
    {
        while ((Vector3.Distance(playerUnit.position, targetPos) > 0.001f)) {

            float step = m_speedFormation * Time.deltaTime; // calculate distance to move
            playerUnit.position = Vector3.MoveTowards(playerUnit.position, targetPos, step);

            yield return new WaitForEndOfFrame();
        }

        yield return new WaitForEndOfFrame();
    }

}
