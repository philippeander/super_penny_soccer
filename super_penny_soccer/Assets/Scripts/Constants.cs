﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Constants
{
    public const string TAG_PLAYER = "PlayerGoalTrigger";
    public const string TAG_OPPONENT = "OpponentGoalTrigger";
    public const string A_POINT_HELPER = "A_pointHelper";
    public const string B_POINT_HELPER = "B_pointHelper";
    public const string C_POINT_HELPER = "C_pointHelper";

    public const string MASK_FIELD = "Field";

    public static LayerMask GetMaskField {
        get {
            return LayerMask.GetMask(Constants.MASK_FIELD);
        }
    }
}
