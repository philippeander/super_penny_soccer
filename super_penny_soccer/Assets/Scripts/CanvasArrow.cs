﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CanvasArrow : MonoBehaviour
{
    static public CanvasArrow Instance;

    public RectTransform arrowPivot;
    public RectTransform circlePresure;
    public Image selectionCircle;
    public Slider slider;

    private List<Image> m_arrowImage = new List<Image>();

    private void Awake()
    {
        Instance = this;
        m_arrowImage.AddRange(GetComponentsInChildren<Image>());
    }

    public void EnableGraphicsElements(bool enable)
    {
        m_arrowImage.ForEach(x => x.enabled = enable);
        selectionCircle.enabled = enable;
    }
}
